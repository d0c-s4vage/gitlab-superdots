let s:this_dir = fnamemodify(resolve(expand('<sfile>:p')), ':h')

let g:UltiSnipsSnippetsDir = s:this_dir."/ultisnippets"
let g:UltiSnipsSnippetDirectories=add(g:UltiSnipsSnippetDirectories, s:this_dir."/ultisnippets")
