# gitlab-superdots

This project is a [superdots](https://github.com/super-dots/superdots) plugin.

* [Description](#description)
* [Installing](#installing)
* [Updating](#updating)

## Description

d0c-s4vage's GitLab specific superdots

## Installing

After installing superdots, record this as a plugin in your ~/.bashrc:

```
source /path/to/superdots/bash_init.sh  # should already be there from installing

superdots https://gitlab.com/d0c-s4vage/gitlab-superdots.git
```

And then run the command below to install the plugin

```
superdots-install
```

## Updating

To update your local copy of this plugin to the latest version, run the command
below:

```
superdots-update
```
