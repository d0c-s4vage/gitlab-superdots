#!/usr/bin/env bash


function reset_headphones {
    dev=$(echo devices | bluetoothctl | grep "Device.*WH-1000XM3" | awk '{print $2}')
    if [ -z "$dev" ] ; then
        superdots-err "Headphones not paired"
        return 1
    fi

    (
        echo disconnect ;
        sleep 5 ;
        echo connect $dev ;
    ) | bluetoothctl
}
