#!/usr/bin/env bash


JOURNALS_DIR=${JOURNALS_DIR:-~/journals}
JOURNALS_YML=${JOURNALS_YML:-true}
JOURNALS_MD=${JOURNALS_MD:-true}


function did {
    if [ $# -lt 1 ] ; then
        echo "USAGE: did [TAGS] message"
        return 1
    fi

    if [ $# -ge 2 ] ; then
        local tags="$1"
        shift
    else
        local tags=""
    fi

    local message="$@"

    if [ ! -d "${JOURNALS_DIR}" ] ; then
        mkdir -p "${JOURNALS_DIR}"
    fi

    local today=$(date +%Y-%m-%d)

    if [ "$JOURNALS_YML" = true ] ; then
        _did_yml "$today" "$tags" "$message"
    fi

    if [ "$JOURNALS_MD" = true ] ; then
        _did_md "$today" "$tags" "$message"
    fi
}


function _append {
    local file="$1"
    local msg="$2"

    echo "$msg" >> "$file"
}


function _append_noeol {
    local file="$1"
    local msg="$2"

    echo -n "$msg" >> "$file"
}


function _did_md {
    local today="$1"
    local tags="$2"
    local message="$3"

    local j="${JOURNALS_DIR}/${today}.md"

    if [ ! -f "$j" ] ; then
        _append "$j" "# ${today}"
    fi
    _append "$j"

    _append_noeol "$j" "## $(date +%Y-%m-%d\ %H:%M:%S\ %z)"
    if [ ! -z "$tags" ] ; then
        for tag in $(sed -e 's/,/ /g' <<<${tags}) ; do
            _append_noeol "$j" " \`${tag}\`"
        done
    fi
    _append "$j"

    _append "$j"
    _append "$j" "> $message"

    echo "Updated ${j}"
}


function _did_yml {
    local today="$1"
    local tags="$2"
    local message="$3"

    local j="${JOURNALS_DIR}/${today}.yml"
    if [ ! -f "$j" ] ; then
        _append "$j" "title: ${today}"
        _append "$j" "entries:"
    fi

    _append "$j" "  - entry: \"${message}\""
    _append "$j" "    date: \"$(date +%Y-%m-%d\ %H:%M:%S\ %z)\""

    if [ -z "${tags}" ] ; then
        _append "$j" "    tags: []"
    else
        _append "$j" "    tags:"
        for tag in $(sed -e 's/,/ /g' <<<${tags}) ; do
            _append "$j" "      - ${tag}"
        done
    fi

    echo "Updated ${j}"
}
