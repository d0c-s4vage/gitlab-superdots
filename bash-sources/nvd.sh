#!/usr/bin/env bash

function nvd_recent {
    curl -s 'https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-recent.json.gz' | gunzip
}

function nvd_year {
    if [ $# -ne 1 ] ; then
        echo "USAGE: nvd_recent YEAR"
        return 1
    fi
    local year="$1"
    curl -s "https://nvd.nist.gov/feeds/json/cve/1.1/nvdcve-1.1-$year.json.gz" | gunzip
}

function nvd_cve {
    if [ $# -ne 1 ] ; then
        echo "USAGE: nvd_cve CVE"
        return 1
    fi
    local cve="$1"
    local year=$(sed -s 's/CVE-//' <<<"$cve" | sed -e 's/-.*//')
    local number=$(sed -s 's/CVE-//' <<<"$cve" | sed -e 's/....-//')
    local cve="CVE-${year}-${number}"
    nvd_year "$year" | ruby <(cat <<-EOF
        require 'json'
        data = JSON.parse(STDIN.read)
        match = data['CVE_Items'].filter{|x| (x['cve']['CVE_data_meta'] || {})['ID'] == '$cve'}
        puts JSON.pretty_generate(match.first)
EOF
)
}

function nvd_cwe {
    if [ $# -ne 1 ] ; then
        echo "USAGE: nvd_cwe CWE"
        return 1
    fi
    local cwe=$(sed -s 's/CWE-//' <<<"$1")
    local cwe_dir="${HOME}/.config/nvd"
    local cwe_csv="${cwe_dir}/cwe.csv"
    if ! [ -d "$cwe_dir" ] ; then
        mkdir -p "$cwe_dir"
    fi
    if ! [ -f "$cwe_csv" ] ; then
        curl -s 'https://cwe.mitre.org/data/csv/1000.csv.zip' > "${cwe_dir}/research.zip"
        unzip "${cwe_dir}/research.zip" -d "${cwe_dir}"
        mv "${cwe_dir}"/{1000,cwe}.csv
    fi

    ruby <(cat <<-EOF
        require 'csv'
        require 'json'
        rows = CSV.read("${cwe_csv}", headers: true)
        rows.each do |row|
            next unless row['CWE-ID'] == '${cwe}'
            puts JSON.pretty_generate(row.to_h)
        end
EOF
)
}

function nvd_cwe_title {
    if [ $# -ne 1 ] ; then
        echo "USAGE: nvd_cwe_title CWE"
        return 1
    fi
    local cwe=$(sed -s 's/CWE-//' <<<"$1")

    curl -s https://cwe.mitre.org/data/definitions/$cwe.html \
        | grep "CWE-${cwe}.*</title" \
        | sed -e 's/^\s*//' \
        | sed 's/(4.0)//' \
        | sed -e 's/\s*<\/title>.*//' \
        | sed -e "s/CWE-${cwe}: //"
}

function nvd_cve_cpes {
    if [ $# -ne 1 ] ; then
        echo "USAGE: nvd_cve CVE"
        return 1
    fi
    local cve="$1"
    nvd_cve "$cve" | fx .configurations
}
