#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"


function some_new_function {
    if [ $# -ne 1 ] ; then
        echo "USAGE: some_new_function ARG1"
        return 1
    fi
    echo "$@"
}
