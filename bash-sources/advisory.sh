#!/usr/bin/env bash


export ADVISORY_DB_HOME="${ADVISORY_DB_HOME:-/home/james/ws/dev/gitlab/advisory-db-curation-tools}"


function advisory_versions {
    if [ $# -ne 2 ] ; then
        echo "USAGE: advisory_versions FIXED AFFECTED"
        return 1
    fi

    fixed="$1"
    affected="$2"
    
    (
        ruby <<-EOF
            require 'yaml'
            require '${ADVISORY_DB_HOME}/adbcurate/lib/field_generator'
            require '${ADVISORY_DB_HOME}/adbcurate/lib/semantic_version/version_range'
            require '${ADVISORY_DB_HOME}/adbcurate/lib/semantic_version/version_parser'
            require '${ADVISORY_DB_HOME}/adbcurate/lib/semantic_version/version_translator'

            fixed = '$fixed'.split(',')
            affected = '$affected'

            affected_versions = VersionRange.new
            affected.split('||').each do |version_range|
                affected_versions << VersionParser.parse(version_range)
            end
            all_versions = affected_versions.version_intervals.flat_map do |x|
                    [x.start_cut, x.end_cut]
                end.filter do |x|
                   not ['-inf', '+inf'].include?(x.to_s)
               end
            all_versions += fixed.map{|x| VersionCut.new(x) }
            all_versions = all_versions.map{|x| x.to_s }.sort.uniq
            
            ret = FieldGenerator.gen_version_info_fields(all_versions, affected_versions.collapse)
            ret2 = {
                'affected_range' => affected,
                'fixed_versions' => ret['fixed_versions'],
                'affected_versions' => affected_versions.to_description_s,
                'not_impacted' => ret['not_impacted'],
                'solution' => ret['solution'],
            }
            puts YAML.dump(ret2, line_width: 81).split("\\n")[1..-1].join("\\n")
EOF
    )
}
