#!/usr/bin/env bash

function side_by_side {
    if [ $# -ne 2 ] || [ $1 == "--help" ]; then
        echo "USAGE: side_by_side DATA1 DATA2"
        return 1
    fi
    cat <<EOF | ruby "$1" "$2"
EOF
}
