function gitlab_changed_files {
    git diff --name-only ${1:-master}
}

function gitlab_be_check_style {
    autofix_arg=""
    if [ "$AUTO_FIX" == 1 ]  ; then autofix_arg="-a" ; fi
    log "────────────────────"
    log "RUBOCOP"
    log "────────────────────"
    bundle exec rubocop $autofix_arg $(gitlab_changed_files $1 | grep "\.rb")
}

function gitlab_be_fix_style {
    AUTO_FIX=1 gitlab_be_check_style "$@"
}

function gitlab_be_check_tests {
    bin/rspec $(gitlab_changed_files $1 | grep "spec.rb")
}

function gitlab_be_check {
    log "======================="
    log "Backend:tests"
    gitlab_be_check_tests "$@" 2>&1 | log_box_indent
    log "======================="
    log "Backend:styles"
    gitlab_be_check_style "$@" 2>&1 | log_box_indent
}

function gitlab_fe_check_tests {
    yarn jest $(gitlab_changed_files $1 | grep spec.js)
}

function gitlab_fe_check_style {
    autofix_arg=""
    log "────────────────────"
    log "ESLINT"
    log "────────────────────"
    if [ "$AUTO_FIX" == 1 ]  ; then autofix_arg="--fix" ; fi
    yarn exec eslint -- $autofix_arg $(gitlab_changed_files $1 | grep -E "\.vue|\.js")

    autofix_arg="-c"
    log "────────────────────"
    log "PRETTIER"
    log "────────────────────"
    if [ "$AUTO_FIX" == 1 ]  ; then autofix_arg="-w" ; fi
    yarn prettier $autofix_arg $(gitlab_changed_files $1 | grep -E "\.vue|\.js")
}

function gitlab_fe_fix_style {
    AUTO_FIX=1 gitlab_fe_check_style "$@"
}

function gitlab_fe_check {
    log "======================="
    log "Frontend:tests"
    gitlab_fe_check_tests "$@" 2>&1 | log_box_indent
    log "======================="
    log "Frontend:styles"
    gitlab_fe_check_style "$@" 2>&1 | log_box_indent
}

function gitlab_check_styles {
    log "======================="
    log "Backend:styles"
    gitlab_be_check_style "$@" 2>&1 | log_box_indent
    log "======================="
    log "Frontend:styles"
    gitlab_fe_check_style "$@" 2>&1 | log_box_indent
}

function gitlab_fix_styles {
    log "======================="
    log "Fixing Backend:styles"
    gitlab_be_fix_style "$@" 2>&1 | log_box_indent
    log "======================="
    log "Fixing Frontend:styles"
    gitlab_fe_fix_style "$@" 2>&1 | log_box_indent
}

function gitlab_check_tests {
    log "======================="
    log "Backend:tests"
    gitlab_be_check_tests "$@" 2>&1 | log_box_indent
    log "======================="
    log "Frontend:tests"
    gitlab_fe_check_tests "$@" 2>&1 | log_box_indent
}

function gitlab_full_check {
    gitlab_check_tests "$@"
    gitlab_check_styles "$@"
}
