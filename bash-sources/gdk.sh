#!/usr/bin/env bash

function gdk_kill_runsv_jobs {
    kill -KILL $(ps aux | grep runsv | grep -v grep | awk '{print $2}')
}

function gdk_kill_background_jobs {
    kill $(ps aux | grep rails-background-jobs | awk '{print $2}')
}

function gdk_restart_background_jobs {
    gdk stop rails-background-jobs
    gdk_kill_background_jobs
    gdk start rails-background-jobs
}

function gdk_full_restart {
    gdk stop
    gdk_kill_background_jobs
    gdk start
}

function gdk_branch_restart {
    bundle install
    yarn install
    gdk reconfigure
    gdk_full_restart
}

function gdk_branch_restart_tail {
    gdk_branch_restart
    gdk tail "$@"
}

function gdk_db_setup {
    bundle exec rake dev:setup RAILS_ENV=development
}

function gdk_rails_background_errors {
    tail -Ff ../log/rails-background-jobs/current | grep error_backtrace | sed 's/^[^{]*//' | json_pp
}
